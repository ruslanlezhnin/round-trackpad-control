//
//  CircularTrackpad.swift
//  Circular Trackpad
//
//  Created by Ruslan on 07.03.2023.
//

import SwiftUI
import AVFoundation

struct CircularTrackpad: View {
    @State private var startLocation: CGPoint
    @State private var dragLocation: CGPoint?
    @State private var isCursorOnEdge: Bool = false
    @State private var soundUrl: URL?
    @State private var player: AVAudioPlayer!

    @Binding var output: CGPoint

    var trackpadSize: CGFloat
    var indicatorSize: CGFloat

    private var radius: CGFloat {
        return trackpadSize / 2
    }

    var dragGesture: some Gesture {
        DragGesture(coordinateSpace: .local)
            .onChanged { value in
                if dragLocation == nil {
                    dragLocation = CGPoint.zero
                }

                if CGPointDistance(from: startLocation, to: value.location) > radius {
                    if !isCursorOnEdge {
                        playSound()
                        isCursorOnEdge = true
                    }
                    calculateEdgePoint(touchPoint: value.location)
                } else {
                    dragLocation = value.location
                    isCursorOnEdge = false
                }

                output = ellipticalDiscToSquare(location: dragLocation!)
            }
    }

    init(trackpadSize: CGFloat, indicatorSize: CGFloat, output: Binding<CGPoint>) {
        self._startLocation = State(initialValue: CGPoint(x: trackpadSize / 2, y: trackpadSize / 2))
        self.trackpadSize = trackpadSize
        self.indicatorSize = indicatorSize
        self._output = output
        self._soundUrl = State(initialValue: Bundle.main.url(forResource: "Cowbell", withExtension: "wav"))

        guard let url = soundUrl else {
            print("URL INIT FAILED")
            return
        }

        do {
            _player = State(initialValue: try AVAudioPlayer(contentsOf: url))
            player.prepareToPlay()
        } catch {
            print(error.localizedDescription)
        }
    }

    var body: some View {
        GeometryReader { reader in
            ZStack {
                // Background circle
                Circle()
                    .fill(.yellow)
                    .gesture(dragGesture)

                // Indicator
                Circle()
                    .fill(.red)
                    .frame(width: indicatorSize)
                    .position(dragLocation ?? startLocation)
            }
            .onAppear {
                startLocation = CGPoint(
                    x: reader.frame(in: .local).midX,
                    y: reader.frame(in: .local).midY)
            }
        }
        .frame(width: trackpadSize, height: trackpadSize, alignment: .center)
    }

    func CGPointDistanceSquared(from: CGPoint, to: CGPoint) -> CGFloat {
        return (from.x - to.x) * (from.x - to.x) + (from.y - to.y) * (from.y - to.y)
    }

    func CGPointDistance(from: CGPoint, to: CGPoint) -> CGFloat {
        return sqrt(CGPointDistanceSquared(from: from, to: to))
    }

    func calculateEdgePoint(touchPoint: CGPoint) {
        let initialHypotenuse = CGPointDistance(from: startLocation, to: touchPoint)
        let TouchC = CGPoint(x: touchPoint.x, y: startLocation.y)
        let initialAdjacent = CGPointDistance(from: startLocation, to: TouchC)
        let alpha = acos(initialAdjacent / initialHypotenuse)

        var x: CGFloat = 0
        var y: CGFloat = 0

        if touchPoint.x <= radius && touchPoint.y <= radius {
            x = radius * (1 - cos(alpha))
            y = radius - radius * sin(alpha)
        } else if touchPoint.x > radius && touchPoint.y <= radius {
            x = radius * (1 + cos(alpha))
            y = radius - radius * sin(alpha)
        } else if touchPoint.x <= radius && touchPoint.y > radius {
            x = radius * (1 - cos(alpha))
            y = radius + radius * sin(alpha)
        }
        else if touchPoint.x > radius && touchPoint.y > radius {
            x = radius * (1 + cos(alpha))
            y = radius + radius * sin(alpha)
        }

        dragLocation = CGPoint(x: x, y: y)
    }

    func ellipticalDiscToSquare(location: CGPoint) -> CGPoint
    {
        let x = Util.remap(from: location.x, fromMin: 0, fromMax: trackpadSize, toMin: -1, toMax: 1)
        let y = Util.remap(from: location.y, fromMin: 0, fromMax: trackpadSize, toMin: -1, toMax: 1)

        let u2 = x * x
        let v2 = y * y
        let twosqrt2 = 2.0 * sqrt(2.0)
        let subtermx = 2.0 + u2 - v2
        let subtermy = 2.0 - u2 + v2
        let termx1 = subtermx + x * twosqrt2
        let termx2 = subtermx - x * twosqrt2
        let termy1 = subtermy + y * twosqrt2
        let termy2 = subtermy - y * twosqrt2

        return CGPoint(
            x: 0.5 * sqrt(termx1) - 0.5 * sqrt(termx2),
            y: 0.5 * sqrt(termy1) - 0.5 * sqrt(termy2)
        )
    }

    func playSound() {
        guard let url = soundUrl else {
            print("URL INIT FAILED")
            return
        }

        do {
            player = try AVAudioPlayer(contentsOf: url)
            player.prepareToPlay()
            player.play()
        } catch {
            print(error.localizedDescription)
        }
    }
}
