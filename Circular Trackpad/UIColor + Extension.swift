//
//  UIColor + Extension.swift
//  Circular Trackpad
//
//  Created by Ruslan on 10.03.2023.
//

import Foundation
import UIKit

extension UIColor {
    func toColor(_ color: UIColor, percentage: CGFloat) -> UIColor {
        let percentage = max(min(percentage, 100), 0) / 100
        switch percentage {
        case 0: return self
        case 1: return color
        default:
            var (r1, g1, b1, a1): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
            var (r2, g2, b2, a2): (CGFloat, CGFloat, CGFloat, CGFloat) = (0, 0, 0, 0)
            guard self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1) else { return self }
            guard color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2) else { return self }

            return UIColor(red: CGFloat(r1 + (r2 - r1) * percentage),
                           green: CGFloat(g1 + (g2 - g1) * percentage),
                           blue: CGFloat(b1 + (b2 - b1) * percentage),
                           alpha: CGFloat(a1 + (a2 - a1) * percentage))
        }
    }

    func add(overlay: UIColor) -> UIColor {
            var bgR: CGFloat = 0
            var bgG: CGFloat = 0
            var bgB: CGFloat = 0
            var bgA: CGFloat = 0

            var fgR: CGFloat = 0
            var fgG: CGFloat = 0
            var fgB: CGFloat = 0
            var fgA: CGFloat = 0

            self.getRed(&bgR, green: &bgG, blue: &bgB, alpha: &bgA)
            overlay.getRed(&fgR, green: &fgG, blue: &fgB, alpha: &fgA)

            let r = fgA * fgR + (1 - fgA) * bgR
            let g = fgA * fgG + (1 - fgA) * bgG
            let b = fgA * fgB + (1 - fgA) * bgB

            return UIColor(red: r, green: g, blue: b, alpha: 1.0)
        }
}
