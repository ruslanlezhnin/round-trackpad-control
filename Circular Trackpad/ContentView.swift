//
//  ContentView.swift
//  Circular Trackpad
//
//  Created by Ruslan on 06.03.2023.
//

import SwiftUI

struct ContentView: View {
    @State var point: CGPoint = CGPoint.zero
    @State var backgroundColor: Color

    init() {
        backgroundColor = Util.pointToColor(point: CGPoint.zero)
    }

    var body: some View {
        ZStack {
            ZStack {
                backgroundColor
                    .ignoresSafeArea()
            }

            CircularTrackpad(trackpadSize: 250, indicatorSize: 30, output: $point)
                .onChange(of: point) { p in
                    backgroundColor = Util.pointToColor(point: p)
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
