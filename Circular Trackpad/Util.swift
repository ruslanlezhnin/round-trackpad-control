//
//  Util.swift
//  Circular Trackpad
//
//  Created by Ruslan on 10.03.2023.
//

import Foundation
import SwiftUI

struct Util {
    static func remap(from: CGFloat, fromMin: CGFloat, fromMax: CGFloat, toMin: CGFloat,  toMax: CGFloat) -> CGFloat
    {
        let fromAbs  =  from - fromMin
        let fromMaxAbs = fromMax - fromMin
        let normal = fromAbs / fromMaxAbs
        let toMaxAbs = toMax - toMin
        let toAbs = toMaxAbs * normal
        let to = toAbs + toMin

        return to
    }

    static func pointToColor(point: CGPoint) -> Color {
        let xPercentage = Util.remap(from: point.x, fromMin: -1, fromMax: 1, toMin: 0, toMax: 100)
        let yPercentage = Util.remap(from: point.y, fromMin: -1, fromMax: 1, toMin: 0, toMax: 1)

        return Color(UIColor.gray.toColor(.red, percentage: xPercentage)).opacity(yPercentage)
    }
}
