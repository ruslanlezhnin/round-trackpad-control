//
//  Circular_TrackpadApp.swift
//  Circular Trackpad
//
//  Created by Ruslan on 06.03.2023.
//

import SwiftUI

@main
struct Circular_TrackpadApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
